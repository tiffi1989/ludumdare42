﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SharkScript : MonoBehaviour
{

    private bool active, goRight;
    private Rigidbody2D rig;
    private Animator ani;
    public float speed;
    public Transform water;
    private GameObject target;
    private ArrayList targets = new ArrayList();
    private GameObject enemyInRange;

    // Use this for initialization
    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        ani = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (water.position.y <= -5.836f || ani.GetBool("attack"))
            return;

        if (enemyInRange != null && !ani.GetBool("attack"))
        {
            ani.SetBool("attack", true);
            targets.Remove(enemyInRange);
            GetComponent<AudioSource>().Play();
            if (enemyInRange.GetComponent<player_controller>() != null)
                enemyInRange.GetComponent<player_controller>().Die();
            else if (enemyInRange.GetComponent<enemyScript>() != null)
                enemyInRange.GetComponent<enemyScript>().Die();
            rig.velocity = new Vector2();
            Invoke("StopAttack", 1f);
            return;
        }

        if (rig.velocity.x > 0)
        {
            ani.SetBool("looksRight", true);
        }
        else
        {
            ani.SetBool("looksRight", false);
        }

        if (targets.Count > 0)
        {
            Vector3 direction = ((GameObject)targets[0]).transform.position - transform.position;
            rig.velocity = new Vector2(direction.x, direction.y).normalized * speed;
        }
        else
        {
            ani.SetBool("attack", false);

            if (transform.position.y >= 0.2f)
            {
                Vector3 direction = new Vector3(0, 0, 0) - transform.position;
                rig.velocity = new Vector2(direction.x, direction.y).normalized * speed;
            }
            else
            {
                if (transform.position.x > 3)
                {
                    rig.velocity = new Vector2(-speed, 0);
                }
                if (transform.position.x < -3)
                {
                    rig.velocity = new Vector2(speed, 0);
                }
            }


        }
    }

    private void StopAttack()
    {
        ani.SetBool("attack", false);
        rig.velocity = new Vector2(speed * (ani.GetBool("looksRight") ? 1 : -1), 0);
    }

    public void SetTarget(GameObject target)
    {
        targets.Add(target);
    }

    public void RemoveTarget(GameObject target)
    {
        targets.Remove(target);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "HitAble" || collision.gameObject.tag == "Player")
        {
            enemyInRange = collision.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "HitAble" || collision.gameObject.tag == "Player")
        {
            enemyInRange = null;
        }
    }
}
