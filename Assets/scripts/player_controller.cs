﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class player_controller : MonoBehaviour
{

    public float speed, maxSpeed, jumpSpeed;
    private Rigidbody2D rig;
    private float timeSinceLastJump;
    private Animator ani;
    public hitBoxScript hitBoxLeft, hitBoxRight;
    private int waterCount;
    public GameObject[] bodyParts;
    private SharkScript shark;
    private bool isDead;
    public Text[] gameOverTexts;

    // Use this for initialization
    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        ani = GetComponent<Animator>();
        shark = GameObject.Find("Shark").GetComponent<SharkScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isDead)
        {
            gameOverTexts[0].color = new Color(gameOverTexts[0].color.r, gameOverTexts[0].color.g, gameOverTexts[0].color.b, gameOverTexts[0].color.a + 0.1f * Time.deltaTime);
            gameOverTexts[1].color = new Color(gameOverTexts[1].color.r, gameOverTexts[1].color.g, gameOverTexts[1].color.b, gameOverTexts[1].color.a + 0.1f * Time.deltaTime);

            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene("main");

            }
            return;
        }
        if (ani.GetBool("swim"))
            MoveUnderwater();
        else
        {
            Move();
            Punch();
        }


    }

    void MoveUnderwater()
    {
        if (Mathf.Abs(rig.velocity.x) < maxSpeed / 3)
        {
            if (Input.GetKey(KeyCode.D))
            {
                rig.AddForce(new Vector2(Time.deltaTime * speed / 2, 0));
                ani.SetBool("looksRight", true);
            }
            if (Input.GetKey(KeyCode.A))
            {
                rig.AddForce(new Vector2(Time.deltaTime * -speed / 2, 0));
                ani.SetBool("looksRight", false);

            }
        }

        rig.AddForce(new Vector2(rig.velocity.x * 10 * -Time.deltaTime, 0));

        if (Input.GetButtonDown("Jump"))
        {
            rig.AddForce(new Vector2(0, jumpSpeed / 10));
            timeSinceLastJump = 0;
        }
    }

    void Move()
    {
        if (Mathf.Abs(rig.velocity.x) < maxSpeed)
        {
            if (Input.GetKey(KeyCode.D))
            {
                rig.AddForce(new Vector2(Time.deltaTime * 2 * speed, 0));
                ani.SetBool("looksRight", true);
            }
            if (Input.GetKey(KeyCode.A))
            {
                rig.AddForce(new Vector2(Time.deltaTime * 2 * -speed, 0));
                ani.SetBool("looksRight", false);

            }
        }



        timeSinceLastJump += Time.deltaTime;
        if (Input.GetAxis("Horizontal") != 0 && !ani.GetBool("jump"))
        {
            ani.SetBool("run", true);
        }
        else
        {
            ani.SetBool("run", false);
        }

        if (Input.GetButton("Jump") && !ani.GetBool("jump"))
        {
            rig.AddForce(new Vector2(0, jumpSpeed));
            timeSinceLastJump = 0;
            ani.SetBool("jump", true);
        }

        if (rig.velocity.y <= 0.05f && rig.velocity.y > -0.05f && timeSinceLastJump > 0.1f)
        {
            ani.SetBool("jump", false);
            ani.SetBool("fall", false);

        }

        if (rig.velocity.y <= -0.05f && ani.GetBool("jump"))
        {
            ani.SetBool("fall", true);
        }
    }

    void Punch()
    {
        if (Input.GetButton("Fire1") && !ani.GetBool("attack1") && !ani.GetBool("attack2"))
        {
            if (ani.GetBool("looksRight"))
                Invoke("PunchLeft", .15f);
            else
                Invoke("PunchRight", .15f);

            ani.SetBool("attack2", true);
            Invoke("StopPunch", .3f);

        }
        if (Input.GetButton("Fire2") && !ani.GetBool("attack1") && !ani.GetBool("attack2"))
        {
            if (ani.GetBool("looksRight"))
                Invoke("PunchLeft", .15f);
            else
                Invoke("PunchRight", .15f);

            ani.SetBool("attack1", true);
            Invoke("StopPunch", .3f);

        }
    }

    void PunchLeft()
    {
        hitBoxRight.Attack();

    }

    void PunchRight()
    {
        hitBoxLeft.HighAttack();

    }

    void StopPunch()
    {
        ani.SetBool("attack2", false);
        ani.SetBool("attack1", false);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Water")
            waterCount++;
        if (collision.gameObject.tag == "Water" && !ani.GetBool("swim"))
        {
            rig.gravityScale = 0f;
            GetComponent<ConstantForce2D>().enabled = true;
            Invoke("IntoWater", .1f);
            ani.SetBool("swim", true);
            ani.SetBool("run", false);
            ani.SetBool("fall", false);
            ani.SetBool("jump", false);
            shark.SetTarget(gameObject);
            GetComponent<AudioSource>().Play();

        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Water")
            waterCount--;
        if (collision.gameObject.tag == "Water" && ani.GetBool("swim") && waterCount == 0)
        {
            rig.gravityScale = 1f;
            GetComponent<ConstantForce2D>().enabled = false;
            ani.SetBool("swim", false);
            rig.velocity = new Vector2(rig.velocity.x, rig.velocity.y / 10);
            shark.RemoveTarget(gameObject);
        }

    }

    void IntoWater()
    {
        rig.velocity = 0.1f * rig.velocity;
    }

    public void Die()
    {
        GetComponent<CapsuleCollider2D>().enabled = false;
        GetComponent<Rigidbody2D>().simulated = false;
        GetComponent<SpriteRenderer>().enabled = false;
        isDead = true;
        foreach (GameObject bodyPart in bodyParts)
        {
            Rigidbody2D tmpRig = Instantiate(bodyPart, transform.position, Quaternion.identity).GetComponent<Rigidbody2D>();
            tmpRig.AddTorque(Random.Range(-.5f, .5f));
            tmpRig.AddForce(new Vector2(Random.Range(-2f, 2f), 0));
        }
    }
}
