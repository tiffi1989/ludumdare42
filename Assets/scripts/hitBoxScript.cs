﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hitBoxScript : MonoBehaviour {

    private float hitForce = 150;
    private GameObject enemyInRange;
    public AudioClip[] clips;
    private AudioSource source;

	// Use this for initialization
	void Start () {
        source = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void EnemyAttack()
    {

    }

    public bool IsSomeoneInRange()
    {
        if (enemyInRange != null)
            return true;
        else return false;
    }

    public void Attack()
    {
        Attack(hitForce);
    }

    public void Attack(float strength)
    {
        if(enemyInRange != null)
        {
            if(enemyInRange.GetComponent<enemyScript>() != null)
                enemyInRange.GetComponent<enemyScript>().Hit();

            source.clip = clips[0];
            source.Play();
            if (transform.position.x > enemyInRange.transform.position.x)
                enemyInRange.GetComponent<Rigidbody2D>().AddForce(new Vector2(-strength, 0));
            else
                enemyInRange.GetComponent<Rigidbody2D>().AddForce(new Vector2(strength, 0));
        }
    }

    public void HighAttack()
    {
        HighAttack(hitForce);
    }

    public void HighAttack(float strength)
    {

        if (enemyInRange != null)
        {
            if (enemyInRange.GetComponent<enemyScript>() != null)
                enemyInRange.GetComponent<enemyScript>().Hit();

            source.clip = clips[1];
            source.Play();
            if (transform.position.x > enemyInRange.transform.position.x)
                enemyInRange.GetComponent<Rigidbody2D>().AddForce(new Vector2(-strength, strength));
            else
                enemyInRange.GetComponent<Rigidbody2D>().AddForce(new Vector2(strength, strength));
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "HitAble" || collision.gameObject.tag == "Player") {
            enemyInRange = collision.gameObject;           
        }            
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "HitAble" || collision.gameObject.tag == "Player")
        {
            enemyInRange = collision.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "HitAble" || collision.gameObject.tag == "Player")
        {
            enemyInRange = null;
        }
    }
}
