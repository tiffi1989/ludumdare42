﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class enemyScript : MonoBehaviour
{

    private bool isBusy, isHit, isJumping, isAttacking, dead;
    private Rigidbody2D rig;
    public float speed, jumpSpeed, strength, maxSpeed;
    public bool isAggressive;
    private string currentAction = "";
    private Animator ani;
    public hitBoxScript leftHitBox, rightHitBox;
    private float swimJumpTimer, standsInTriggerTimer;
    private int waterCount;
    public GameObject[] bodyParts;
    private SharkScript shark;


    // Use this for initialization
    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        ani = GetComponent<Animator>();
        shark = GameObject.Find("Shark").GetComponent<SharkScript>();
        currentAction = UnityEngine.Random.Range(0, 2) == 0 ? "MoveRight" : "MoveLeft";
    }

    // Update is called once per frame
    void Update()
    {
        if (!ani.GetBool("swim"))
        {
            rig.gravityScale = 1f;
            shark.RemoveTarget(gameObject);
        }
        else
            rig.gravityScale = 0f;

        if (rig.velocity.y < 0.1f && rig.velocity.y > -0.1f && ani.GetBool("fall"))
        {
            isHit = false;
            ani.SetBool("jump", false);
            ani.SetBool("fall", false);

            if (currentAction == "JumpRight") currentAction = "MoveRight";
            else if (currentAction == "JumpLeft") currentAction = "MoveLeft";

        }
        else if (rig.velocity.y < 0 && !ani.GetBool("swim"))
        {
            ani.SetBool("fall", true);
        }

        if ((ani.GetBool("looksRight") ? rightHitBox : leftHitBox).IsSomeoneInRange() && !isAttacking && !ani.GetBool("swim"))
        {
            Invoke("Attack", UnityEngine.Random.Range(0f, 1f));
            isAttacking = true;
        }


        switch (currentAction)
        {
            case "MoveRight": Move(true); break;
            case "MoveLeft": Move(false); break;
            case "Swim": Swim(); break;

            case "Stop": rig.velocity = new Vector2(0, rig.velocity.y); break;
            default:
                break;
        }

    }

    void Swim()
    {
        swimJumpTimer += Time.deltaTime;
        if (swimJumpTimer > 1f)
        {
            rig.AddForce(new Vector2(0, jumpSpeed / 10));
            swimJumpTimer = 0;
        }
    }

    void Move(bool moveRight)
    {
        ani.SetBool("run", true);

        if (moveRight)
        {
            if (rig.velocity.x < maxSpeed)
                rig.AddForce(new Vector2(speed * 2 * Time.deltaTime, 0));
            ani.SetBool("looksRight", true);

        }
        else
        {
            if (rig.velocity.x > -maxSpeed)
                rig.AddForce(new Vector2(-speed * 2 * Time.deltaTime, 0));
            ani.SetBool("looksRight", false);

        }
    }

    void Attack()
    {
        currentAction = "Attacking";
        switch (UnityEngine.Random.Range(0, 7))
        {
            case 0:
            case 1:
            case 2:
                (ani.GetBool("looksRight") ? rightHitBox : leftHitBox).Attack(strength);
                ani.SetBool("attack2", true);
                break;
            case 3:
            case 4:
            case 5:
                (ani.GetBool("looksRight") ? rightHitBox : leftHitBox).HighAttack(strength);
                ani.SetBool("attack1", true);
                break;
            case 6: currentAction = ani.GetBool("looksRight") ? "MoveRight" : "MoveLeft"; break;
            default: break;
        }
        ani.SetBool("run", false);
        ani.SetBool("jump", false);
        ani.SetBool("swim", false);
        ani.SetBool("fall", false);
        Invoke("StopAttack", 0.3f);
    }

    void StopAttack()
    {
        currentAction = ani.GetBool("looksRight") ? "MoveRight" : "MoveLeft";
        ani.SetBool("run", false);
        ani.SetBool("jump", false);
        ani.SetBool("swim", false);
        ani.SetBool("fall", false);
        ani.SetBool("attack2", false);
        ani.SetBool("attack1", false);
        isAttacking = false;


    }

    public void Hit()
    {
        isHit = true;
        Invoke("DeactivateHit", 0.2f);
    }

    void DeactivateHit()
    {
        isHit = false;
    }

    void Jump(bool highJump)
    {
        if (ani.GetBool("swim"))
            return;
        currentAction = "Jump" + (ani.GetBool("looksRight") ? "Right" : "Left");
        ani.SetBool("jump", true);
        if (highJump)
            rig.AddForce(new Vector2(speed / 5 * (ani.GetBool("looksRight") ? 1 : -1), jumpSpeed));
        else
            rig.AddForce(new Vector2(speed / 3f * (ani.GetBool("looksRight") ? 1 : -1), jumpSpeed / 1.5f));

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((currentAction == "JumpRight" || currentAction == "JumpLeft") && collision.gameObject.tag != "Water")
            return;
        string[] triggerNames = { "LeftJumpTrigger", "RightJumpTrigger", "FlatJumpTrigger", "HighJumpTrigger", "LeftDeadEndTrigger", "RightDeadEndTrigger" };
        if (Array.Exists(triggerNames, element => element == collision.gameObject.tag) && !isHit)
        {
            TriggerActive(collision);
        }
        if (collision.gameObject.tag == "Water")
            waterCount++;
        if (collision.gameObject.tag == "Water" && !ani.GetBool("swim"))
        {
            GetComponent<AudioSource>().Play();
            rig.gravityScale = 0f;
            GetComponent<ConstantForce2D>().enabled = true;
            Invoke("IntoWater", .1f);
            ani.SetBool("swim", true);
            ani.SetBool("run", false);
            ani.SetBool("fall", false);
            ani.SetBool("jump", false);
            currentAction = "Swim";
            shark.SetTarget(gameObject);
        }
    }

    void TriggerActive(Collider2D collision)
    {
        if (!isHit)
            rig.velocity = new Vector2(0, 0);
        ani.SetBool("run", false);
        ani.SetBool("jump", false);
        ani.SetBool("swim", false);
        ani.SetBool("fall", false);
        ani.SetBool("attack1", false);
        ani.SetBool("attack2", false);
        switch (collision.gameObject.tag)
        {
            case "FlatJumpTrigger":
                Jump(false);
                break;
            case "HighJumpTrigger":
                Jump(true);
                break;
            case "LeftJumpTrigger":
                if (!ani.GetBool("looksRight")) Jump(true);
                break;
            case "RightJumpTrigger": if (ani.GetBool("looksRight")) Jump(true); break;
            case "LeftDeadEndTrigger": currentAction = "MoveRight"; break;
            case "RightDeadEndTrigger": currentAction = "MoveLeft"; break;
            default: break;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        standsInTriggerTimer += Time.deltaTime;
        string[] triggerNames = { "FlatJumpTrigger", "HighJumpTrigger", "LeftDeadEndTrigger", "RightDeadEndTrigger" };
        if (Array.Exists(triggerNames, element => element == collision.gameObject.tag) && !isHit && standsInTriggerTimer > 1)
        {
            standsInTriggerTimer = 0;
            TriggerActive(collision);
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Water")
            waterCount--;
        if (collision.gameObject.tag == "Water" && ani.GetBool("swim") && waterCount == 0)
        {
            shark.RemoveTarget(gameObject);
            standsInTriggerTimer = 0;
            rig.gravityScale = 1f;
            GetComponent<ConstantForce2D>().enabled = false;
            ani.SetBool("swim", false);
            rig.velocity = new Vector2(rig.velocity.x, rig.velocity.y / 10);
            Jump(UnityEngine.Random.Range(0, 2) == 1 ? true : false);

        }

    }

    void IntoWater()
    {
        rig.velocity = 0.1f * rig.velocity;
    }

    public bool IsDead()
    {
        return dead;
    }

    public void Die()
    {
        GetComponent<CapsuleCollider2D>().enabled = false;
        GetComponent<Rigidbody2D>().simulated = false;
        GetComponent<SpriteRenderer>().enabled = false;
        dead = true;
        foreach (GameObject bodyPart in bodyParts)
        {
            Rigidbody2D tmpRig = Instantiate(bodyPart, transform.position, Quaternion.identity).GetComponent<Rigidbody2D>();
            tmpRig.AddTorque(UnityEngine.Random.Range(-.5f, .5f));
            tmpRig.AddForce(new Vector2(UnityEngine.Random.Range(-2f, 2f), 0));
        }
    }
}
