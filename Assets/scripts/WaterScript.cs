﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterScript : MonoBehaviour {

    public Transform waterOutside, waterInside;
    public float waterSpeed;
    

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (waterInside.localPosition.y <= waterOutside.localPosition.y)
            waterInside.localPosition = new Vector3(waterInside.localPosition.x, waterInside.localPosition.y + waterSpeed * Time.deltaTime, waterInside.localPosition.z);
        else
            transform.position = new Vector3(transform.position.x, transform.position.y + waterSpeed * Time.deltaTime * transform.localScale.y, transform.position.z);
    }
}
