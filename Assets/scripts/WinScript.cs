﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class WinScript : MonoBehaviour {

    private bool playerInWinzone;
    public enemyScript[] enemies;
    private float counter;
    public Text text;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        counter += Time.deltaTime;
        if(counter > .5f)
        {
            counter = 0;
            int enemiesAlive = 0;
            foreach(enemyScript enemy in enemies)
            {
                if(!enemy.IsDead())
                {
                    enemiesAlive++;
                }
            }
            if (enemiesAlive > 0)
                text.text = "Enemies remaining: " + enemiesAlive;
            else
                text.text = "Go to the Crows Nest!";
            if(playerInWinzone && enemiesAlive == 0)
            {
                SceneManager.LoadScene("win");

            }
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            playerInWinzone = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerInWinzone = false;
        }
    }
}
